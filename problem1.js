// declare findUserInterestedInPlayingVideoGames function
function findUserInterestedInPlayingVideoGames(users){

    // if the object not have any key means it's empty
    if(Object.keys(users).length === 0){
        // so return null
        return null;
    }

    // get all the keys form the users object and store in keysArray
    const keysArray = Object.keys(users);

    // use reduce method to filter data which user interests is Video Games
    const result = keysArray.reduce( (acc, curr) => {
        // get current interests and convert into a string
        let interests = users[curr].interests.toString();

        // check the interests is Video Games or not
        if(interests.includes("Video Games")){
            // if yes then add as a object with keys & value in acc array
            acc[curr] = users[curr];
        }
        
        // return acc array to next step
        return acc;
    }, {});

    // return the result
    return result;
}

// export the findUserInterestedInPlayingVideoGames
module.exports = findUserInterestedInPlayingVideoGames;