// declare sortTheUsers function
function sortTheUsers(users){

    // if the object not have any key means it's empty
    if(Object.keys(users).length === 0){
        // so return null
        return null;
    }

    // get all the keys form the users object and store in keysArray
    const keysArray = Object.keys(users);

    // create seniorityLevel object
    const seniorityLevel = {
        "Senior" : 1,
        "Developer" : 2,
        "Intern" : 3
    };

    // use sort method to sort with comapre function
    const result = keysArray.sort( (a,b) => {
        // get the desgination
        const user1Desgination = users[a].desgination.split(" ")[0];
        const user2Desgination = users[b].desgination.split(" ")[0];

        // get the biggest seniority level
        const seniorityLevelComparison = seniorityLevel[user1Desgination] - seniorityLevel[user2Desgination];

        // if the seniorityLevelComparison is not 0 means both role are different
        if(seniorityLevelComparison !== 0){
            // so return seniorityLevelComparison
            return seniorityLevelComparison;
        }

        // if both role(Designation) are same sort using age 
        return users[b].age - users[a].age;

    }).reduce( (acc, curr) => {
        // add object in acc
        acc[curr] = users[curr];
        // return acc array to next step
        return acc;
    }, {});

    // return the result
    return result;
}

// export the sortTheUsers function
module.exports = sortTheUsers;