// declare the groupTheUsers function
function groupTheUsers(users){


    // if the object not have any key means it's empty
    if(Object.keys(users).length === 0){
        // so return null
        return null;
    }

    // get all the keys form the users object and store in keysArray
    const keysArray = Object.keys(users);

    const result = keysArray.reduce( (acc, curr) => {

        const desgination = users[curr].desgination.toString();

        // check the desgination have Javascript word
        if(desgination.includes("Javascript")){
            // if yes then now check that acc object have Javascript key or not
            if(acc.hasOwnProperty("Javascript")){
                // if Javascript key already present then update with old users + new users
                const oldValue = acc["Javascript"];
                acc['Javascript'] = [oldValue + " , " + curr];
            }
            else{
                // if key not present in object then create new key & value pairs of array type
                acc['Javascript'] = [curr];
            }
        }
        // check the desgination have Golang word
        else if(desgination.includes("Golang")){
            // if yes then now check that acc object have Golang key or not
            if(acc.hasOwnProperty("Golang")){
                // if Golang key already present then update with old users + new users
                const oldValue = acc["Golang"];
                acc['Golang'] = [oldValue + " , " + curr];
            }
            else{
                // if key not present in object then create new key & value pairs of array type
                acc['Golang'] = [curr];
            }
        }
        else{
            if(acc.hasOwnProperty("Python")){
                // if Python key already present then update with old users + new users
                const oldValue = acc["Python"];
                acc['Python'] = [oldValue + " , " + curr];
            }
            else{
                // if key not present in object then create new key & value pairs of array type
                acc['Python'] = [curr];
            }
        }
        

        return acc;
    }, {});

    // return the result
    return result;
}

// export the groupTheUsers function
module.exports = groupTheUsers;