// import the findUserInterestedInPlayingVideoGames function
const findUserInterestedInPlayingVideoGames = require('../problem1.js');
// import the users object
const users = require('../usersData.js');

// call the findUserInterestedInPlayingVideoGames function
const result = findUserInterestedInPlayingVideoGames(users);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the object is empty
    console.log("The object is empty");
}