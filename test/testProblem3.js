// import the sortTheUsers function
const sortTheUsers = require('../problem3.js');
// import the users object
const users = require('../usersData.js');

// call the sortTheUsers function
const result = sortTheUsers(users);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the object is empty
    console.log("The object is empty");
}