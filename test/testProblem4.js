// import the findUsersWithMastersDegree function
const findUsersWithMastersDegree = require('../problem4.js');
// import the users object
const users = require('../usersData.js');

// call the findUsersWithMastersDegree function
const result = findUsersWithMastersDegree(users);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the object is empty
    console.log("The object is empty");
}