// import the groupTheUsers function
const groupTheUsers = require('../problem5.js');
// import the users object
const users = require('../usersData.js');

// call the groupTheUsers function
const result = groupTheUsers(users);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the object is empty
    console.log("The object is empty");
}