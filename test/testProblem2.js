// import the findUsersStayingInGermany function
const findUsersStayingInGermany = require('../problem2.js');
// import the users object
const users = require('../usersData.js');

// call the findUsersStayingInGermany function
const result = findUsersStayingInGermany(users);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the object is empty
    console.log("The object is empty");
}