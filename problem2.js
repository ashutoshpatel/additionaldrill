// declare findUsersStayingInGermany function
function findUsersStayingInGermany(users){

    // if the object not have any key means it's empty
    if(Object.keys(users).length === 0){
        // so return null
        return null;
    }

    // get all the keys form the users object and store in keysArray
    const keysArray = Object.keys(users);

    // use reduce method to filter data which user nationality is Germany
    const result = keysArray.reduce( (acc, curr) => {
        // check for the current object value that nationality is Germany or not
        if(users[curr].nationality == "Germany"){
            // if yes then add as a object with keys & value in acc array
            acc[curr] = users[curr];
        }
        
        // return acc array to next step
        return acc;
    }, {});

    // return the result
    return result;
}

// export the findUsersStayingInGermany function
module.exports = findUsersStayingInGermany;